---
marp: true
backgroundColor: #FFEFC0
style: |
  section {
    justify-content: start;
  }
---
# Microservices : Outbox pattern 📤


---
# Contexte 🖼

* Architecture Microservices (données fortement liées)
* Evènementiel
* Asynchrone


---
![bg w:1200](./imgs/outbox%20context.png)


---
# Problème 📉

* Que se passe-t-il si le message n'a pas pu être envoyé ?

  - Broker down
  - Erreur au runtime côté service

---
![bg w:1200](./imgs/outbox%20context%20fail.png)


---
# Problème 📉

* La transaction en base a déjà eu lieu

* ➡ Information perdue

* ➡ Incohérence des données

---
# Objectif 🎯⛳

* Garantir l'intégrité des transactions

* Cohérence entre les microservices

* Tout en restant dans un mode asynchrone

---
# Solution proposée ✅

- Stockage des messages dans une table en base de données

- Un processus asynchrone récupère les messages non-publiés et s'occupe de les publier dans le broker

| created             | message       | published |
|---------------------|---------------|-----------|
| 2023-03-30 10:00:00 | ORDER_CREATED | true      |
| 2023-04-30 10:01:00 | ORDER_UPDATED | true      |
| 2023-04-30 10:02:00 | ORDER_UPDATED | false     |

---
![bg w:1000](./imgs/outbox%20solution.png)


---
# Code actuel

<!-- 
Dans l'idéal, MessageService.send() est asynchrone :
- Mauvaise pratique de bloquer la transaction base de données par un appel réseau
- La transaction BD et l'envoi de message doivent être 2 processus atomiques et séparés
 -->

```java
public class OrderService {
  @Transactional
  public void createOrder(Order order) {
    this.orderRepository.save(order);
    this.messageService.send("ORDER_CREATED", order.getId());
  }
}
```


---
# Solution proposée ✅

```java
public class OrderService {
  @Transactional
  public void createOrder(Order order) {
    this.orderRepository.save(order);
    this.outboxRepository.save("ORDER_CREATED")
  }
}

public class OutboxService {
  @Scheduled
  public void publishNewMessages() {
    this.outboxRepository.findNewMessages().forEach(message -> {
      this.messageService.send(message);
      message.setPublished(true);
    });
  }
}
```

---
# Sources

- [Pattern: Transactional outbox](https://microservices.io/patterns/data/transactional-outbox.html)
- [Outbox pattern in Spring Boot](https://medium.com/@egorponomarev/outbox-pattern-in-spring-boot-8e8cf116f044)
